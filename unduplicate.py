#!/usr/bin/env python3

'''
Un-Duplicate files found
'''

import os
import re
import sys
import time
import pprint
import hashlib
import argparse

RELEASE_NB = '0.1.0'
RELEASE_DATE = '2018-01-03'

FOLDERS_IGNORED = ('.git', 'CVS', 'py2', 'py3', '__pycache__')
FILES_IGNORED = ('.gitignore')
MIN_FILESIZE = 1024  # files processed have to be >1KB

BLOCK_READ_SIZE = 1048576  # 1024 * 1024

def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('--folder', action='append', default=[], dest='folders', help='Folder to browse for duplicates')
    parser.add_argument('--extension', action='append', default=[], dest='extensions', help='Files extension to check for duplicates (if none given then all files are checked)')
    parser.add_argument('--allow-rm-in', action='append', default=[], help='Folder in which duplicates can be removed')
    parser.add_argument('--auto-rm', action='store_const', default=False, const=True, help='Remove duplicate without asking for confirmation before')
    options = parser.parse_args()
    if options.folders == []:
        print('ERROR: At least 1 folder has to be specified.', file=sys.stderr)
        parser.print_help()
        sys.exit(1)

    # Ensure there is one trailing / behind every --allow-rm-in options
    for i in range(len(options.allow_rm_in)):
        options.allow_rm_in[i] = options.allow_rm_in[i].rstrip('/') + '/'
    return options

def scan_folder(folder, options, files_md5, files_permission_error, extensions_not_tested):
    '''
    scan folder according to FOLDERS_IGNORED, FILES_IGNORED, options.extensions
    store it in files_md5 as {'md5': ['file_path', ...]}
    '''

    def extension_of(filename):
        '''
        return the extension (might be with several part of max 4 chars on the right of the dot)
        '''
        parts = filename.split('.')
        parts = parts[1:]
        extension = ''
        for part in reversed(parts):
            if len(part) <= 4:
                extension = '.{}{}'.format(part, extension)
            else:
                break
        return extension[1:].lower()


    for root, dirs, files in os.walk(folder, topdown=True):
        for directory in list(dirs):
            if directory in FOLDERS_IGNORED:
                dirs.remove(directory)
        for filename in files:
            if filename not in FILES_IGNORED:
                # check file extension
                if options.extensions != []:
                    scan_file = False
                    for ext in options.extensions:
                        if re.match(r'.*\.{}$'.format(ext), filename, re.I):
                            scan_file = True
                            break
                    if not scan_file:
                        extensions_not_tested.add(extension_of(filename))
                        continue
                full_filename = os.path.join(root, filename)
                stat = os.stat(full_filename, follow_symlinks=False)
                if stat.st_size < MIN_FILESIZE:
                    continue
                m = hashlib.md5()
                try:
                    with open(full_filename, 'rb') as f:
                        while True:
                            file_bytes = f.read(BLOCK_READ_SIZE)
                            if file_bytes != b'':
                                m.update(file_bytes)
                            else:
                                break
                    files_md5.setdefault(m.hexdigest(), []).append(full_filename)
                except PermissionError:
                    files_permission_error.append(full_filename)
                except FileNotFoundError:
                    pass  # broken symlink

def second_step_hash_files(files_md5, files_md5_and_sha1):
    '''
    parses files_md5 dict.
    for files that are unique in a md5, skip them
    for files that have md5 collision, do also calculate their sha1 and store
    them in files_md5_and_sha1 as {('md5', 'sha1'): ['file_path', ...]}
    Finally remove entries that are unique (which would have same MD5 but different SHA1)
    '''
    for md5, filenames in files_md5.items():
        if len(filenames) > 1:
            for filename in filenames:
                m = hashlib.sha1()
                with open(filename, 'rb') as f:
                    while True:
                        file_bytes = f.read(BLOCK_READ_SIZE)
                        if file_bytes != b'':
                            m.update(file_bytes)
                        else:
                            break
                stat = os.stat(filename, follow_symlinks=False)
                files_md5_and_sha1.setdefault((md5, m.hexdigest(), stat.st_size), []).append({'filename': filename})

    for infos, files in list(files_md5_and_sha1.items()):
        if len(files) < 2:
            del files_md5_and_sha1[infos]

def sizeof_fmt(num, suffix='B'):
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return '%3.1f%s%s' % (num, unit, suffix)
        num /= 1024.0
    return '%.1f%s%s' % (num, 'Yi', suffix)

def select_candidates_to_rm(files_md5_and_sha1, options):
    space_lost = {
        'to_recover': 0,
        'total_waste': 0,
    }
    for sig, files in files_md5_and_sha1.items():
        space_lost['total_waste'] += sig[2] * (len(files) - 1)
        nb_files_to_rm = 0
        for fil in files:
            fil['to_rm'] = False
            fil['mtime'] = os.path.getmtime(fil['filename'])
            for folder in options.allow_rm_in:
                if re.match(r'^{}.*$'.format(folder), fil['filename']):
                    fil['to_rm'] = True
                    nb_files_to_rm += 1
        if nb_files_to_rm == len(files):
            # if all the files are candidate to rm, then keep the one with oldest mtime
            oldest_i = 0
            for i, fil in enumerate(files):
                if fil['mtime'] < files[oldest_i]['mtime']:
                    oldest_i = i
            files[oldest_i]['to_rm'] = False
            nb_files_to_rm -= 1
        space_lost['to_recover'] += sig[2] * nb_files_to_rm
    return space_lost

def do_rm(files_md5_and_sha1):
    loop_again = True
    while loop_again:
        loop_again = False
        for files in files_md5_and_sha1.values():
            for fil in files:
                if fil['to_rm']:
                    try:
                        os.unlink(fil['filename'])
                        fil['to_rm'] = False
                    except FileNotFoundError:
                        pass
                    except PermissionError:
                        print('E: Could not rm {}'.format(fil['filename']))
                        loop_again = True
        if loop_again:
            answer = input('Do you want to try again? ([Y/n])')
            if answer.lower() == 'n':
                loop_again = False

if __name__ == '__main__':
    print('Un-Duplicate!  (version {} - {})'.format(RELEASE_NB, RELEASE_DATE))
    options = get_options()

    files_md5 = {}
    files_permission_error = []
    extensions_not_tested = set()
    for folder in options.folders:
        scan_folder(folder, options, files_md5, files_permission_error, extensions_not_tested)

    files_md5_and_sha1 = {}
    second_step_hash_files(files_md5, files_md5_and_sha1)

    if len(files_permission_error) != 0:
        print('The following files could not be read :')
        print('\n'.join(files_permission_error))
    if len(files_md5_and_sha1) != 0:
        space_lost = select_candidates_to_rm(files_md5_and_sha1, options)
        print('The following {} duplicates were found :'.format(len(files_md5_and_sha1)))
        for infos, files in files_md5_and_sha1.items():
            print('+ {}'.format(sizeof_fmt(infos[2])))
            for fil in files:
                if fil['to_rm']:
                    bullet = '-'
                    color = '\033[31m'
                else:
                    bullet = '+'
                    color = '\033[32m'
                print('  {} {} {}{}\033[0m'.format(bullet, time.ctime(fil['mtime']), color, fil['filename']))
            print()

        print('+ \033[32mgreen\033[0m files are kept\n- \033[31mred\033[0m one are to be deleted')
        print('Which would save in total : {} (in a total waste of {})'.format(sizeof_fmt(space_lost['to_recover']), sizeof_fmt(space_lost['total_waste'])))

        decision_to_rm = False
        if space_lost['to_recover'] != 0:
            if options.auto_rm:
                print()
                for count in range(5, 0, -1):
                    print('\rGonna rm these in {}s. (ctrl-c to quit)'.format(count), end='')
                    time.sleep(1)
                decision_to_rm = True
                print('\r                                      ')
            else:
                answer = input('Do you confirm suppression? ([y/N]) ')
                if answer.lower() == 'y':
                    decision_to_rm = True
        if decision_to_rm:
            print('Starting to rm')
            do_rm(files_md5_and_sha1)
            print('Un-Duplicate completed with success.')
        else:
            print('Nothing deleted.')
    else:
        print('No duplicate were found. You lucky!')
    if len(extensions_not_tested) != 0:
        print('By the way, the files with the following extensions were not tested : ')
        all_extensions = ['']
        for extension in sorted(extensions_not_tested):
            last_line = all_extensions[-1] + extension + ' '
            if len(last_line) > 80:
                all_extensions.append(extension)
            else:
                all_extensions[-1] = last_line
        print('\n'.join(all_extensions))
